--[[
	Copyright 2019 Lucas Schwiderski

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
--]]

return {
	mod_description = {
		en = "mod_compatibility description",
	},

	-- Commands
	command_descr_check = {
		en = "Check mod compatibilities."
	},
	command_descr_master = {
		en = "Get or set master URL."
	},
	command_descr_revision = {
		en = "Print the current release version and revision. Neccessary for incompatibility reports."
	},
	mod_compat_report_view_title = {
		en = "Mod Compatibility Report",
	},

	-- Options
	option_mod_compat_report_view_toggle_title = {
		en = "[DEBUG] Toggle Mod Compat Report View",
	},
	otion_mod_compat_report_view_toggle_tooltip = {
		en = "",
	},
	option_mod_compat_auto_disable_title = {
		en = "Auto-Disable Mods",
	},
	option_mod_compat_auto_disable_tooltip = {
		en = "If this option is checked, mods that are incompatible with the current patch will be disabled automatically.\nYou can aleays re-enable them in the game launcher.",
	},
	option_mod_compat_source_title = {
		en = "Data Source",
	},
	option_mod_compat_source_tooltip = {
		en = "Choose which data source to use for compatibility data.\n\n- Workshop Tag: A tag on the workshop item. Updated by the mod's author and Fatshark\n- Master: A master file hosted on GitHub. Updated by the mod's author, Fatshark and the modding community",
	},
	mod_compat_source_option_workshop = {
		en = "Workshop Tag",
	},
	mod_compat_source_option_master = {
		en = "Master",
	},
	mod_compat_source_option_both = {
		en = "Both",
	},

	-- View strings
	summary_disable = {
		en = "The following mods have been flagged as incompatible. Mods that are incompatible with the current patch will be disabled.",
	},
	summary_report = {
		en = "The following mods have been flagged as incompatible. You should disable them via the game launcher.",
	},
	error_game_incompatible = {
		en = "Incompatible with the current patch.",
	},
	error_mod_incompatible = {
		en = "Incompatible with '%s'.",
	},
	error_create_entries_before_master_loaded = {
		en = "Can't create entries before loading the master.",
	},
	info_reloading_mods_started = {
		en = "Disabling mods in the background...",
	},
}
