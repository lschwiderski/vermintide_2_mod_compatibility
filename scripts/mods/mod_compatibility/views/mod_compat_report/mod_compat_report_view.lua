--[[
  Copyright 2019 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("mod_compatibility")

local GAME_REVISION = tonumber(script_data.settings.content_revision)
local DEFINITIONS = mod:dofile("scripts/mods/mod_compatibility/views/mod_compat_report/mod_compat_report_view_definitions")

local ModCompatReportView = class()

function ModCompatReportView:init(ingame_ui_context)
	self.ui_renderer = ingame_ui_context.ui_renderer
	self.ui_top_renderer = ingame_ui_context.ui_top_renderer
	self.ingame_ui = ingame_ui_context.ingame_ui
	self.render_settings = { snap_pixel_positions = true }

	local input_manager = ingame_ui_context.input_manager
	input_manager:create_input_service("mod_compat_report_view", "IngameMenuKeymaps", "IngameMenuFilters")
	input_manager:map_device_to_service("mod_compat_report_view", "keyboard")
	input_manager:map_device_to_service("mod_compat_report_view", "mouse")
	input_manager:map_device_to_service("mod_compat_report_view", "gamepad")
	self.input_manager = input_manager

	local world = ingame_ui_context.world_manager:world(VT1 and "music_world" or "level_world")
	self.wwise_world = Managers.world:wwise_world(world)

	self._master_loaded = false
	self.master = {}

	self._mod_list = Application.user_setting("mods")

	self:create_ui_elements()

	mod:debug("Mod Compatibility Report View initialized.")
end

function ModCompatReportView:destroy()
	mod:debug("Mod Compatibility Report View destroyed.")
end

function ModCompatReportView:on_enter(transition_params)
	mod:debug("Mod Compatibility Report View opened. transition_params: %s", transition_params)
	if ShowCursorStack.stack_depth == 0 then
		ShowCursorStack.push()
	end

	local input_manager = self.input_manager
	input_manager:block_device_except_service("mod_compat_report_view", "keyboard", 1)
	input_manager:block_device_except_service("mod_compat_report_view", "mouse", 1)
	input_manager:block_device_except_service("mod_compat_report_view", "gamepad", 1)

	self:_load_master()

	self:play_sound("Play_hud_button_open")
end

function ModCompatReportView:on_exit(transition_params)
	mod:debug("Mod Compatibility Report View closed. transition_params: %s", transition_params)
	ShowCursorStack.pop()

	local input_manager = self.input_manager
	input_manager:device_unblock_all_services("keyboard", 1)
	input_manager:device_unblock_all_services("mouse", 1)
	input_manager:device_unblock_all_services("gamepad", 1)

	self:play_sound("Play_hud_button_close")

	if mod:get("mod_compat_auto_disable") then
		self:_disable_incompatible_mods()
	end
end

function ModCompatReportView:input_service()
	return self.input_manager:get_service("mod_compat_report_view")
end

function ModCompatReportView:create_ui_elements()
	self.scenegraph = UISceneGraph.init_scenegraph(DEFINITIONS.scenegraph)

	self._loading_icon = UIWidget.init(DEFINITIONS.loading_widgets.loading_icon)

	self.window_widgets = {}
	for widget_name, widget_definition in pairs(DEFINITIONS.window_widgets) do
		self.window_widgets[widget_name] = UIWidget.init(widget_definition)
	end

	self.content_widgets = {}
	for widget_name, widget_definition in pairs(DEFINITIONS.content_widgets) do
		self.content_widgets[widget_name] = UIWidget.init(widget_definition)
	end

	if not mod:get("mod_compat_auto_disable") then
		local summary_text_widget = self.content_widgets.summary_text
		summary_text_widget.content.text = mod:localize("summary_report")
	end

	self._scrollbar = ScrollBarLogic:new(self.content_widgets.mod_list_scrollbar)
end

function ModCompatReportView:update(dt, t)
	self:_update_animations(dt)
	self:draw(dt)
	self:_handle_input(dt, t)
end

function ModCompatReportView:_update_animations(dt)
	UIWidgetUtils.animate_default_button(self.window_widgets.exit_button, dt)
end

local function get_font_height(gui, style)
	local font, calculated_font_size = UIFontByResolution(style)
	local _, _, font_name = unpack(font)
	return UIGetFontHeight(gui, font_name, calculated_font_size)
end

function ModCompatReportView:draw(dt)
	local ui_renderer = self.ui_renderer
	local render_settings = self.render_settings
	local scenegraph = self.scenegraph
	local input_service = self:input_service()

	UIRenderer.begin_pass(ui_renderer, scenegraph, input_service, dt, nil, render_settings)

	for _, widget in pairs(self.window_widgets) do
		UIRenderer.draw_widget(ui_renderer, widget)
	end

	if self._loading or not self._master_loaded then
		self:rotate_loading_icon(dt)
		UIRenderer.draw_widget(ui_renderer, self._loading_icon)
	else
		for _, widget in pairs(self.content_widgets) do
			UIRenderer.draw_widget(ui_renderer, widget)
		end

		if self.list_widgets then
			for _, widget in pairs(self.list_widgets) do
				UIRenderer.draw_widget(ui_renderer, widget)
			end
		end
	end

	UIRenderer.end_pass(ui_renderer)
end

function ModCompatReportView:_handle_input(dt, t)
	local input_service = self:input_service()
	local gamepad_active = Managers.input:is_device_active("gamepad")

	local widgets = self.window_widgets
	local exit_button = widgets.exit_button

	if input_service:get("toggle_menu") or self:_is_button_pressed(exit_button) or (gamepad_active and input_service:get("back")) then
		self:close_menu()
	end

	if self:_is_button_hover_enter(exit_button) then
		self:play_sound("play_gui_equipment_button_hover")
	end

	local scrollbar = self._scrollbar
	scrollbar:update(dt, t, false)
	self.scenegraph.mod_list_root.position[2] = scrollbar:get_scrolled_length()
end

function ModCompatReportView:rotate_loading_icon(dt)
	local loading_icon_style = self._loading_icon.style.texture_id
	local angle_fraction = loading_icon_style.fraction or 0
	angle_fraction = (angle_fraction + dt) % 1
	local anim_fraction = math.easeOutCubic(angle_fraction)
	local angle = anim_fraction * math.degrees_to_radians(360)
	loading_icon_style.angle = angle
	loading_icon_style.fraction = angle_fraction
end

function ModCompatReportView:play_sound(event)
	WwiseWorld.trigger_event(self.wwise_world, event)
end

function ModCompatReportView:close_menu()
	self.ingame_ui:handle_transition("exit_menu")
end

function ModCompatReportView:_is_button_hover_enter(widget)
	return widget.content.button_hotspot.on_hover_enter or false
end

function ModCompatReportView:_is_button_pressed(widget)
	local content = widget.content
	local hotspot = content.button_hotspot or content.hotspot

	if hotspot.on_release then
		hotspot.on_release = false

		return true
	end

	return false
end

function ModCompatReportView:_get_master(cb)
	local master_url = mod:get("master_url")

	if not master_url then
		return cb({ message = "No master URL. Please run \"compat_master\" first!" })
	end

	local curl_manager = Managers.curl
	curl_manager:get(master_url, nil, function(success, code, _, data)
		if not success then
			return cb({ message = "Failed to retrieve master file. Code: " .. code })
		end

		return cb(nil, cjson.decode(data))
	end)
end

function ModCompatReportView:_apply_workshop_flags_to_master(master)
	for _, mod_data in pairs(self._mod_list) do
		if mod_data.enabled and table.find(mod_data.tags or {}, "incompatible") then
			local id = mod_data.id
			local compat_data = master[id] or {}
			compat_data.revision = GAME_REVISION
			master[id] = compat_data
		end
	end

	return master
end

function ModCompatReportView:_load_master()
	self._loading = true

	self:_get_master(function(err, master)
		if err then
			mod:error(err.message)
			return
		end

		self.master = self:_apply_workshop_flags_to_master(master)
		self._master_loaded = true
		self._loading = false
		self:_create_entries()
	end)
end

function ModCompatReportView:_create_entries()
	if not self._master_loaded then
		mod:error("error_create_entries_before_master_loaded")
		return
	end

	local ui_renderer = self.ui_renderer
	local masked = true
	local widget_definition = DEFINITIONS.create_report_entry_definition("mod_list_entry", masked)
	local list_widgets = {}
	local list_widget_offset = 10
	local list_spacing = 15

	for mod_id, compat_data in pairs(self.master) do
		repeat
			local lines = {}

			if compat_data.revision and tonumber(compat_data.revision) >= GAME_REVISION then
				table.insert(lines, mod:localize("error_game_incompatible"))
			end

			local mod_list = compat_data.mods
			if mod_list and #mod_list > 0 then
				for _, incompatible_mod_id in ipairs(mod_list) do
					table.insert(lines, mod:localize("error_mod_incompatible", self:_get_mod_name_from_workshop_id(incompatible_mod_id)))
				end
			end

			if #lines <= 0 then
				break
			end

			local list_widget = UIWidget.init(widget_definition)
			local content = list_widget.content
			local style = list_widget.style

			content.text.lines = lines
			content.mod_name = self:_get_mod_name_from_workshop_id(mod_id)
			local background_content = content.background
			local background_texture_settings = background_content.background_texture_settings

			local frame_style = style.frame
			local mod_name_style = style.mod_name
			local mod_name_shadow_style = style.mod_name_shadow
			local divider_style = style.divider
			local multiline_text_style = style.text
			local multiline_text_shadow_style = style.text_shadow
			local background_style = style.background
			local background_fade_style = style.background_fade

			-- Calculate height of multiline_text
			local multiline_text_spacing = multiline_text_style.spacing
			local font_height = get_font_height(ui_renderer.gui, multiline_text_style)

			local line_height = font_height + multiline_text_spacing
			local multiline_text_height = #lines * line_height
			-- No clue why, but the offset needs to be reduced by 1/2 of the text height
			local multiline_text_offset = multiline_text_height - (font_height / 2)

			multiline_text_style.size[2] = multiline_text_height
			multiline_text_style.offset[2] = multiline_text_offset

			multiline_text_shadow_style.size[2] = multiline_text_height
			multiline_text_shadow_style.offset[2] = multiline_text_offset - 2

			-- Calculate height of full content
			local mod_name_height = mod_name_style.size[2]
			local divider_height = divider_style.size[2]
			local widget_height = multiline_text_height + mod_name_height + divider_height
			local background_height = widget_height - 4

			frame_style.size[2] = widget_height
			background_style.size[2] = background_height
			background_content.uvs[2][2] = math.min((background_height - 50) / background_texture_settings.size[2], 1)
			background_fade_style.texture_size[2] = background_height

			-- Move components in widget based on height of multiline_text
			local mod_name_offset_y = widget_height - mod_name_height
			mod_name_style.offset[2] = mod_name_offset_y
			mod_name_shadow_style.offset[2] = mod_name_offset_y - 2
			divider_style.offset[2] = mod_name_offset_y

			-- Move the widget below the previous ones
			list_widget_offset = list_widget_offset + widget_height
			list_widget.offset[2] = -list_widget_offset

			-- Finally, register the widget
			table.insert(list_widgets, list_widget)

			list_widget_offset = list_widget_offset + list_spacing
		until true
	end

	self.list_widgets = list_widgets

	local content_length = list_widget_offset

	local scrollbar = self._scrollbar
	local mod_list_window_height = DEFINITIONS.mod_list_size[2]
	local draw_length = mod_list_window_height
	local scrollbar_length = mod_list_window_height
	local step_size = 220
	local scroll_step_multiplier = 1

	scrollbar:set_scrollbar_values(draw_length, content_length, scrollbar_length, step_size, scroll_step_multiplier)
	scrollbar:set_scroll_percentage(0)
end

function ModCompatReportView:_get_mod_name_from_workshop_id(id)
	for _, mod_data in pairs(self._mod_list) do
		if mod_data.id == id then
			return mod_data.name
		end
	end
end

function ModCompatReportView:_disable_incompatible_mods()
	local need_reload = false

	for _, mod_data in pairs(self._mod_list) do
		local id = mod_data.id
		local compat_data = self.master[id]

		if mod_data.enabled and compat_data and compat_data.revision and tonumber(compat_data.revision) >= GAME_REVISION then
			mod:debug("Disabling '%s' (%s)", self:_get_mod_name_from_workshop_id(id), id)
			mod_data.enabled = false
			need_reload = true
		end
	end

	Application.set_user_setting("mods", self._mod_list)
	Application.save_user_settings()

	if need_reload then
		mod:_trigger_mod_reload()
	end
end

return ModCompatReportView
