--[[
  Copyright 2019 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("mod_compatibility")

local window_size = { 780, 820 }
local window_background_size = window_size
local window_background_color = { 255, 200, 200, 100 }

local window_frame_width = 22
local scrollbar_width = 16
local scrollbar_spacing = 10

local content_size = {
	window_size[1] - (window_frame_width * 2),
	window_size[2] - (window_frame_width * 2),
}

local summary_text_height = 65
local mod_list_window_offset = 20

local mod_list_size = {
	content_size[1],
	content_size[2] - (summary_text_height + mod_list_window_offset),
}

local mod_list_entry_size = { content_size[1] - (20 + scrollbar_width + scrollbar_spacing), 0 }

local content_fade_height = 10

local title_text_style = {
	use_shadow = true,
	upper_case = true,
	localize = false,
	font_size = 28,
	horizontal_alignment = "center",
	vertical_alignment = "center",
	dynamic_font_size = true,
	font_type = "hell_shark_header",
	text_color = Colors.get_color_table_with_alpha("font_title", 255),
	offset = { 0, 0, 2 },
}

local summary_text_style = {
	use_shadow = true,
	upper_case = false,
	localize = false,
	font_size = 24,
	horizontal_alignment = "left",
	vertical_alignment = "top",
	word_wrap = true,
	font_type = "hell_shark",
	text_color = Colors.get_color_table_with_alpha("white", 255),
	offset = { 0, 0, 2 },
}

local scenegraph = {
	root = {
		is_root = true,
		size = {
			1920,
			1080
		},
		position = {
			0,
			0,
			UILayer.default
		}
	},
	menu_root = {
		vertical_alignment = "center",
		parent = "root",
		horizontal_alignment = "center",
		size = {
			1920,
			1080
		},
		position = {
			0,
			0,
			0
		}
	},
	screen = {
		scale = "fit",
		size = {
			1920,
			1080
		},
		position = {
			0,
			0,
			UILayer.default
		}
	},
	console_cursor = {
		vertical_alignment = "center",
		parent = "screen",
		horizontal_alignment = "center",
		size = {
			1920,
			1080
		},
		position = {
			0,
			0,
			0
		}
	},
	window = {
		vertical_alignment = "center",
		parent = "screen",
		horizontal_alignment = "center",
		size = window_size,
		position = { 0, 0, 0 },
	},
	window_background = {
		vertical_alignment = "center",
		parent = "window",
		horizontal_alignment = "center",
		size = {
			window_size[1] - 5,
			window_size[2] - 5
		},
		position = { 0, 0, 0 },
	},
	window_background_mask = {
		vertical_alignment = "center",
		parent = "window",
		horizontal_alignment = "center",
		size = {
			window_size[1] - 5,
			window_size[2] - 5
		},
		position = { 0, 0, 1 },
	},
	exit_button = {
		vertical_alignment = "bottom",
		parent = "window",
		horizontal_alignment = "center",
		size = { 300, 42 },
		position = { 0, -16, 42 },
	},
	title = {
		vertical_alignment = "top",
		parent = "window",
		horizontal_alignment = "center",
		size = {
			658,
			60
		},
		position = {
			0,
			34,
			46
		}
	},
	title_bg = {
		vertical_alignment = "top",
		parent = "title",
		horizontal_alignment = "center",
		size = {
			410,
			40
		},
		position = {
			0,
			-15,
			-1
		}
	},
	title_text = {
		vertical_alignment = "center",
		parent = "title",
		horizontal_alignment = "center",
		size = {
			350,
			50
		},
		position = {
			0,
			-3,
			2
		}
	},
	loading_icon = {
		vertical_alignment = "center",
		horizontal_alignment = "center",
		parent = "window",
		size = { 50, 50 },
		position = { 0, 0, 0 },
	},
	content_window = {
		vertical_alignment = "center",
		horizontal_alignment = "left",
		parent = "window",
		size = content_size,
		position = {
			window_frame_width,
			-window_frame_width,
			0
		},
	},
	summary_text = {
		vertical_alignment = "top",
		horizontal_alignment = "left",
		parent = "content_window",
		size = { content_size[1] - 20, summary_text_height },
		position = { 10, 10, 0 },
	},
	summary_divider = {
		vertical_alignment = "bottom",
		horizontal_alignment = "center",
		parent = "content_window",
		size = { content_size[1], 5 },
		-- Align this with `mod_list_size` so it's always flush with the top of the scrollbar chain
		position = { 0, mod_list_size[2] + mod_list_window_offset, 15 },
	},
	mod_list_window = {
		vertical_alignment = "bottom",
		horizontal_alignment = "left",
		parent = "window",
		size = mod_list_size,
		position = { 22, mod_list_window_offset, 0 },
	},
	mod_list_mask = {
		vertical_alignment = "center",
		parent = "mod_list_window",
		horizontal_alignment = "center",
		size = mod_list_size,
		position = { 0, 0, 3 },
	},
	mod_list_scrollbar = {
		vertical_alignment = "center",
		parent = "mod_list_mask",
		horizontal_alignment = "right",
		size = { scrollbar_width, mod_list_size[2] },
		position = { -scrollbar_spacing, 0, 10 },
	},
	mod_list_root = {
		vertical_alignment = "top",
		horizontal_alignment = "left",
		parent = "mod_list_mask",
		size = { 0, 0 },
		position = { 0, 0, 0 },
	},
	mod_list_entry = {
		vertical_alignment = "top",
		horizontal_alignment = "left",
		parent = "mod_list_root",
		size = mod_list_entry_size,
		position = { 10, 0, 0 },
	},
}

local function create_mod_list_mask(scenegraph_id, size, fade_height)
	fade_height = fade_height or 20

	local passes = {
		{
			pass_type = "hotspot",
			content_id = "hotspot"
		},
		{
			pass_type = "texture",
			style_id = "mask",
			texture_id = "mask_texture"
		},
		{
			pass_type = "texture",
			style_id = "mask_top",
			texture_id = "mask_edge"
		},
		{
			pass_type = "rotated_texture",
			style_id = "mask_bottom",
			texture_id = "mask_edge"
		}
	}

	local content = {
		mask_texture = "mask_rect",
		mask_edge = "mask_rect_edge_fade",
		hotspot = {}
	}

	local style = {
		mask = {
			vertical_alignment = "center",
			horizontal_alignment = "center",
			texture_size = { size[1], size[2] - fade_height * 2 },
			color = { 255, 255, 255, 255 },
			offset = { 0, 0, 0 },
		},
		mask_top = {
			vertical_alignment = "top",
			horizontal_alignment = "center",
			texture_size = { size[1], fade_height },
			color = { 255, 255, 255, 255 },
			offset = { 0, 0, 0 },
		},
		mask_bottom = {
			vertical_alignment = "bottom",
			horizontal_alignment = "center",
			texture_size = { size[1], fade_height },
			color = { 255, 255, 255, 255 },
			offset = { 0, 0, 0 },
			angle = math.pi,
			pivot = { size[1] / 2, fade_height / 2 },
		},
	}

	return {
		element = {
			passes = passes,
		},
		content = content,
		style = style,
		offset = { 0, 0, 0 },
		scenegraph_id = scenegraph_id,
	}
end

local disable_with_gamepad = true
local window_widgets = {
	window = UIWidgets.create_frame("window", scenegraph.window.size, "menu_frame_11", 40),
	background_fade = UIWidgets.create_simple_texture("options_window_fade_01", "window", nil, nil, nil, 2),
	window_background = UIWidgets.create_tiled_texture("window_background", "menu_frame_bg_01", window_background_size, nil, nil, window_background_color),
	window_background_mask = UIWidgets.create_tiled_texture("window_background_mask", "menu_frame_bg_01", window_background_size, nil, true),
	-- Close button at the bottom
	exit_button = UIWidgets.create_default_button(
		"exit_button",
		scenegraph.exit_button.size,
		nil,
		nil,
		Localize("menu_close"),
		24,
		nil,
		"button_detail_04",
		34,
		disable_with_gamepad
	),
	title = UIWidgets.create_simple_texture("frame_title_bg", "title"),
	title_bg = UIWidgets.create_background("title_bg", scenegraph.title_bg.size, "menu_frame_bg_02"),
	title_text = UIWidgets.create_simple_text(mod:localize("mod_compat_report_view_title"), "title_text", nil, nil, title_text_style),
}

local function create_report_entry_definition(scenegraph_id, masked)
	local frame_name = "menu_frame_08"
	local frame_settings = UIFrameSettings[frame_name]

	local retained = false

	local background_texture = "talent_tree_bg_01"
	local background_texture_settings = UIAtlasHelper.get_atlas_settings_by_texture_name(background_texture)

	local divider_texture = "menu_frame_12_divider"

	local outer_spacing = 15

	local size = mod_list_entry_size

	local frame_offset = { 0, 0, 4 }

	local background_size = { size[1] - 4, size[2] - 4 }
	local background_offset = { 2, 2, 0 }

	local mod_name_size = { size[1] - (outer_spacing * 2), 48 }
	local mod_name_offset = { outer_spacing, 0, 2 }

	local divider_size = { size[1] - 4, 5 }
	local divider_offset = { 2, 0, 3 }

	local text_font_size = 24
	local text_size = { size[1] - (outer_spacing * 2), 34 }
	local text_offset = { outer_spacing, 0, 2 }

	local passes = {
		-- background
		{
			pass_type = "texture_uv",
			content_id = "background",
			style_id = "background",
		},
		{
			pass_type = "texture",
			texture_id = "background_fade",
			style_id = "background_fade",
		},
		-- group frame
		{
			pass_type = "texture_frame",
			style_id = "frame",
			texture_id = "frame"
		},
		-- mod name
		{
			style_id = "mod_name",
			pass_type = "text",
			text_id = "mod_name"
		},
		{
			style_id = "mod_name_shadow",
			pass_type = "text",
			text_id = "mod_name"
		},
		-- title divider
		{
			texture_id = "divider",
			style_id = "divider",
			pass_type = "texture",
			retained_mode = retained
		},
		-- TODO: divider "holders". see UITooltipPasses.item_titles
		-- text
		{
			pass_type = "multiline_text",
			lines_id = "text",
			style_id = "text",
		},
		{
			pass_type = "multiline_text",
			lines_id = "text",
			style_id = "text_shadow",
		},
	}
	local content = {
		frame = frame_settings.texture,
		mod_name = "Vermintide Mod Framework",
		divider = divider_texture,
		text = {
			lines = {},
		},
		background = {
			uvs = {
				{ 0, 0 },
				{
					math.min(size[1] / background_texture_settings.size[1], 1),
					math.min((size[2] - 50) / background_texture_settings.size[2], 1),
				},
			},
			background_texture_settings = background_texture_settings,
			texture_id = background_texture,
		},
		background_fade = "options_window_fade_01",
	}
	local style = {
		frame = {
			masked = masked,
			size = size,
			texture_size = frame_settings.texture_size,
			texture_sizes = frame_settings.texture_sizes,
			color = { 255, 255, 255, 255 },
			offset = frame_offset,
		},
		mod_name = {
			font_size = 28,
			upper_case = true,
			localize = false,
			dynamic_font_size = true,
			font_type = (masked and "hell_shark_masked") or "hell_shark",
			text_color = Colors.get_color_table_with_alpha("font_default", 255),
			size = mod_name_size,
			offset = mod_name_offset,
		},
		mod_name_shadow = {
			font_size = 28,
			upper_case = true,
			localize = false,
			dynamic_font_size = true,
			font_type = (masked and "hell_shark_masked") or "hell_shark",
			text_color = Colors.get_color_table_with_alpha("black", 255),
			size = mod_name_size,
			offset = {
				mod_name_offset[1] + 2,
				mod_name_offset[2] - 2,
				mod_name_offset[3] - 1,
			},
		},
		divider = {
			masked = masked,
			size = divider_size,
			color = { 255, 255, 255, 255 },
			offset = divider_offset,
		},
		text = {
			font_size = text_font_size,
			spacing = 2,
			upper_case = false,
			localize = false,
			font_type = (masked and "hell_shark_masked") or "hell_shark",
			text_color = Colors.get_color_table_with_alpha("font_default", 255),
			size = text_size,
			offset = text_offset,
		},
		text_shadow = {
			font_size = text_font_size,
			spacing = 2,
			upper_case = false,
			localize = false,
			font_type = (masked and "hell_shark_masked") or "hell_shark",
			text_color = Colors.get_color_table_with_alpha("black", 255),
			size = text_size,
			offset = {
				text_offset[1] + 2,
				text_offset[2] - 2,
				text_offset[3] - 1,
			},
		},
		background = {
			masked = masked,
			color = { 255, 37, 32, 30 },
			offset = background_offset,
			size = background_size,
		},
		background_fade = {
			masked = masked,
			texture_size = background_size,
			color = { 255, 255, 255, 255 },
			offset = {
				background_offset[1],
				background_offset[2],
				background_offset[3] + 1,
			},
		},
	}

	return {
		element = {
			passes = passes,
		},
		content = content,
		style = style,
		offset = { 0, 0, 0 },
		scenegraph_id = scenegraph_id,
	}
end

local loading_widgets = {
	loading_icon = UIWidgets.create_simple_rotated_texture("matchmaking_connecting_icon", 0, { 25, 25 }, "loading_icon"),
}

local content_widgets = {
	summary_text = UIWidgets.create_simple_text(mod:localize("summary_disable"), "summary_text", nil, nil, summary_text_style),
	summary_divider = UIWidgets.create_simple_texture("menu_frame_12_divider", "summary_divider"),
	mod_list_mask = create_mod_list_mask("mod_list_mask", scenegraph.mod_list_mask.size, content_fade_height),
	mod_list_scrollbar = UIWidgets.create_chain_scrollbar("mod_list_scrollbar", "mod_list_mask", scenegraph.mod_list_scrollbar.size),
}

local generic_input_actions = {
	{
		input_action = "confirm",
		priority = 2,
		description_text = "input_description_select"
	},
	{
		input_action = "back",
		priority = 3,
		description_text = "input_description_close"
	}
}

return {
	scenegraph = scenegraph,
	window_widgets = window_widgets,
	loading_widgets = loading_widgets,
	content_widgets = content_widgets,
	create_report_entry_definition = create_report_entry_definition,
	generic_input_actions = generic_input_actions,
	console_cursor_definition = UIWidgets.create_console_cursor("console_cursor"),
	mod_list_size = mod_list_size,
}
