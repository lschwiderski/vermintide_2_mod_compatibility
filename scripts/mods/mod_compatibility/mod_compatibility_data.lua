--[[
  Copyright 2019 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("mod_compatibility")

return {
	name = "mod_compatibility",
	description = mod:localize("mod_description"),
	is_togglable = true,
	options = {
		widgets = {
			{
				setting_id = "mod_compat_source",
				title = "option_mod_compat_source_title",
				tooltip = "option_mod_compat_source_tooltip",
				type = "dropdown",
				default_value = "both",
				options = {
					{ text = "mod_compat_source_option_workshop", value = "workshop" },
					{ text = "mod_compat_source_option_master", value = "master" },
					{ text = "mod_compat_source_option_both", value = "both" },
				},
			},
			{
				setting_id = "mod_compat_auto_disable",
				title = "option_mod_compat_auto_disable_title",
				tooltip = "option_mod_compat_auto_disable_tooltip",
				type = "checkbox",
				default_value = true,
			},
		},
	},
}
