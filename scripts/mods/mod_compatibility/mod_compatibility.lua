--[[
  Copyright 2019 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("mod_compatibility")

mod._opened_report_view = false

function mod:_trigger_mod_reload()
	-- The mod reloading needs to be deferred after the view has already closed.
	-- Otherwise VMF will deregister the view transitions before they occurr, which leads to a crash.
	mod._need_mod_reload = true
end

function mod:_trigger_report_view()
	if mod._opened_report_view then
		return
	end
	-- Opening the report view needs to be deferred until after the fade transition of the main menu.
	mod._open_report_view = true
end

mod.update = function()
	if mod._need_mod_reload then
		mod:echo(mod:localize("info_reloading_mods_started"))
		Managers.mod:_reload_mods()
		mod._need_mod_reload = false
	end

	if mod._open_report_view and not Managers.transition:in_fade_active() then
		mod:debug("Opening report view...")
		mod:handle_transition("mod_compat_report_view_open", true, true)
		mod._open_report_view = false
		mod._opened_report_view = true
	end
end

local GAME_REVISION = tonumber(script_data.settings.content_revision)

mod:command("compat_revision", mod:localize("command_descr_revision"), function(...)
	mod:echo("Release Version: %s\nRelease Revision: %s", VersionSettings.version, GAME_REVISION)
end)

mod:command("compat_master", mod:localize("command_descr_master"), function(master_url)
	if not master_url then
		local current_master_url = mod:get("master_url")
		if current_master_url then
			mod:echo("Current master URL: '%s'", mod:get("master_url"))
		else
			mod:error("No master URL. Use this command to set one.")
		end

		return
	end

	-- TODO: Validate URL
	mod:set("master_url", master_url)
end)

mod:hook_safe(StartMenuView, "on_exit", function()
	mod:_trigger_report_view()
end)

local ModCompatReportView = mod:dofile("scripts/mods/mod_compatibility/views/mod_compat_report/mod_compat_report_view")

local view_data = {
	view_name = "mod_compat_report_view",
	view_settings = {
		init_view_function = function(ingame_ui_context)
			return ModCompatReportView:new(ingame_ui_context)
		end,
		active = {
			inn = true,
			ingame = false,
		},
		blocked_transitions = {
			inn = {},
			ingame = {},
		},
	},
	view_transitions = {
		mod_compat_report_view_open = function(ingame_ui, _)
			ingame_ui.menu_active = true
			ingame_ui.current_view = "mod_compat_report_view"
		end,
		mod_compat_report_view_close = function(ingame_ui, _)
			ingame_ui.menu_active = false
			ingame_ui.current_view = nil
		end,
	},
}

mod:register_view(view_data)

UIPasses.multiline_text = {
	init = function (pass_definition)
		assert(pass_definition.lines_id, "no lines_id in pass definition. YOU NEEDS IT.")

		return {
			lines_id = pass_definition.lines_id
		}
	end,
	draw = function (ui_renderer, pass_data, _, _, ui_style, ui_content, position, size)
		local font, calculated_font_size = UIFontByResolution(ui_style)
		local font_material, font_size, font_name = unpack(font)
		font_size = calculated_font_size
		local lines = (ui_content[pass_data.lines_id] and ui_content[pass_data.lines_id].lines) or ui_content.lines

		for i = 1, #lines, 1 do
			local line = lines[i]

			if ui_style.localize then
				line = Localize(line)
			end

			if ui_style.upper_case then
				line = TextToUpper(line)
			end

			local width, height = UIRenderer.text_size(ui_renderer, line, font_material, font_size)
			local alignment_offset = Vector3(0, 0, 0)

			if ui_style.horizontal_alignment == "center" then
				alignment_offset[1] = size[1] * 0.5 - width * 0.5
			elseif ui_style.horizontal_alignment == "right" then
				alignment_offset[1] = size[1] - width
			end

			if ui_style.vertical_alignment == "center" then
				alignment_offset[2] = size[2] * 0.5 - height * 0.5
			elseif ui_style.vertical_alignment == "top" then
				alignment_offset[2] = size[2] - height
			end

			UIRenderer.draw_text(ui_renderer, line, font_material, font_size, font_name, position + alignment_offset, ui_style.text_color)

			position[2] = position[2] - height - ui_style.spacing
		end
	end
}
