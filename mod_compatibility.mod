return {
	run = function()
		fassert(rawget(_G, "new_mod"), "`mod_compatibility` mod must be lower than Vermintide Mod Framework in your launcher's load order.")

		new_mod("mod_compatibility", {
			mod_script       = "scripts/mods/mod_compatibility/mod_compatibility",
			mod_data         = "scripts/mods/mod_compatibility/mod_compatibility_data",
			mod_localization = "scripts/mods/mod_compatibility/mod_compatibility_localization",
		})
	end,
	packages = {
		"resource_packages/mod_compatibility/mod_compatibility",
	},
}
